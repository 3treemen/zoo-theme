<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package ZOO_Theme
 */

?>

	<footer id="colophon" class="site-footer">
		<?php if ( is_active_sidebar( 'home_three' ) ) : ?>
			<div id="home_three" class="home_primary_widget">
				<?php dynamic_sidebar( 'home_three' ); ?>
			</div>
		<?php endif; ?>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
