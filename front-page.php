<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ZOO_Theme
 */

get_header();
?>

	<section id="about">
		<h2>Over ZOO</h2>
			<?php if ( is_active_sidebar( 'home_one' ) ) : ?>
				<div id="home_one" class="home_primary_widget">
					<?php dynamic_sidebar( 'home_one' ); ?>
				</div>
			<?php endif; ?>
		<div class="call2action">
				<h4>Meer weten?</h4>
				<div class="buttons">
					<button><a href="tel:+31621592131">Bel me!</a></button>	of 
					<button><a href="mailto:ronnie@zoo.nl">Mail me</a></button>
				</div>
			</div>
	</section>
	<section id="examples">
				<?php if( function_exists( 'home_examples' ) ) {
					home_examples();
				}
				?>
	</section>
	<section id="testimonials">
				<?php if( function_exists( 'home_testimonials' ) ) {
					home_testimonials();
				}
				?>
		</section>
		<section id="contact">
		<h2>Contact</h2>
			<?php if ( is_active_sidebar( 'home_two' ) ) : ?>
				<div id="home_two" class="home_primary_widget">
					<?php dynamic_sidebar( 'home_two' ); ?>
				</div>
			<?php endif; ?>
		
	</section>
<?php
get_sidebar();
get_footer();
